#!/bin/sh

if docker pull pihole/pihole| grep 'Image is up to date'; then
	echo '-> Image is up to date, doing nuffin'
else
	echo 'New image found'
	echo '-> Cleaning up old pihole'
	docker rm -f pihole
	echo '-> Running compose with the new image'
	docker-compose up -d
fi
