# Pi-Hole
Totally a rip-off from the official [https://github.com/pi-hole/docker-pi-hole](https://github.com/pi-hole/docker-pi-hole) repo with few tweaks and additions for personal use.

## Installation
Set up the `WEBPASSWORD.env`-file so that it contains `WEBPASSWORD=the-ui-password`

### Ubuntu specific
The port 53 is in use by `systemd-resolved`.

stop and disable the service 
```bash
systemctl stop systemd-resolved
systemctl disable systemd-resolved
```

edit your DNS server manually to `/etc/resolv.conf` so your host can resolve DNS.

## Running
`docker-compose up -d`

## Updating
`update_image.sh` should check whether there is anything to update and do just that. The process is manual.

## Testing and cleaning stuff up
Running the `docker-compose up` does not alter settings once they have set up on the pi-hole. For this you have to clear the local volumes using `clean-volumes.sh` (which just deletes them).

## Configuring the OpenWRT

### Add static lease to your Raspberry
`Network -> DHCP and DNS -> Static Leases`

Add hostname (raspi) for your raspberry. Note down the IP.

### Configure router to use pi-hole as DNS server
`Network -> Interfaces -> WAN -> Edit`

on `Interfaces >> WAN`-dialog:
`Advanced Settings -> Use custom DNS servers`: Add your Raspi IP

Apply settings

## Windows notifications
First install [BurntToast](https://github.com/Windos/BurntToast) to enable notifications.

For nice picture put the `raspberry.png` to good path and edit the `send-message.sh` to match.

Use `check-for-updates.sh` to check for new updates to the pi-hole docker image.
