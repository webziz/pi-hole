#!/bin/sh

if ssh raspi pi-hole/update-image.sh| grep 'Image is up to date'; then
	ALSA=1
else
        echo 'New image found'
        echo '-> notify'
	./send-message.sh "New pi-hole image downloaded!"
fi
